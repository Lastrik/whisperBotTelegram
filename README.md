# Telegram Whisper Bot
A small bot to transform voice messages to text

# Getting started
To get started you should start by copying the .env.example and set your values there.

Then, you can simply startup the bot from `docker-compose` :
```
docker-compose up -d
```

# To do

- Real error handling
- Make a working loading indication