#!/usr/bin/env python

import logging
import os
import json
import openai
import random
from dotenv import load_dotenv
from telegram import __version__ as TG_VER
from pydub import AudioSegment

load_dotenv()

# Initalisation of values
TG_TOKEN = os.getenv("TG_TOKEN")
openai.organization = os.getenv("OPENAI_ORG")
openai.api_key = os.getenv("OPENAI_API_KEY")

ALLOWED_USERNAMES = json.loads(os.getenv("ALLOWED_USERS"))

try:
    from telegram import __version_info__
except ImportError:
    __version_info__ = (0, 0, 0, 0, 0)  # type: ignore[assignment]

if __version_info__ < (20, 0, 0, "alpha", 1):
    raise RuntimeError(
        f"This example is not compatible with your current PTB version {TG_VER}. To view the "
        f"{TG_VER} version of this example, "
        f"visit https://docs.python-telegram-bot.org/en/v{TG_VER}/examples.html"
    )
from telegram import ForceReply, Update, constants
from telegram.ext import Application, CommandHandler, ContextTypes, MessageHandler, filters

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments update and
# context.
async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a message when the command /start is issued."""
    user = update.effective_user
    if(user.username in ALLOWED_USERNAMES):    
        await update.message.reply_html(
            rf"Hello {user.mention_html()}!, je suis un bot qui peut retranscrire tes messages vocaux en texte . Il suffit de me transférer ton message et je vais te renvoyer le texte approximatif de son contenu",
            reply_markup=ForceReply(selective=True),
        )
    else:
        await update.message.reply_html(
            rf"Hello {user.mention_html()}!, je suis un bot qui peut retranscrire des messages vocaux en texte . Cependant, tu ne fais pas partie des utilisateurs·ices autorisé·es à m'employer. Tu peux demander à ma créatrice de t'ajouter à la liste :)",
            reply_markup=ForceReply(selective=True),
        )

async def voice(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:

    # Check that user is in allowed list :
    if (update.effective_user.username in ALLOWED_USERNAMES):
        # Get the voice memo
        voice = await update.message.voice.get_file()

        # Reply waiting message
        waitMSG = await update.message.reply_text("En cours de transcription...")
        await update.message.reply_chat_action(constants.ChatAction.TYPING)

        # Grab its name
        fileName = update.message.voice.file_id + ".mp3"

        logger.info("Processing new voice memo : " + fileName)

        # Write it to disk
        voiceFilePath = await voice.download_to_drive();

        # Convert it to mp3
        voiceAS = AudioSegment.from_file(open(voiceFilePath,"rb"));
        VF = voiceAS.export(fileName, format="mp3")

        # Send it to openai for transcription
        #voiceFile=open(voiceFilePath,"rb")
        transcript = openai.Audio.transcribe("whisper-1", VF )

        logger.info("Done with : " + fileName)
        # Return the content as a reply message
        text = transcript["text"]
        if (text == ""):
            await update.message.reply_text("Pas de contenu trouvé dans ton message vocal")
        else:
            await update.message.reply_text(text)

        # Remove waiting message and chat action
        await waitMSG.delete()

        # remove files
        os.remove(fileName)
        os.remove(voiceFilePath)

    else:
        await update.message.replay_text("Déso, tu es pas autorisé·e·x à utiliser ce bot (ça coûte cher et tout de le faire tourner)")

def main() -> None:
    
    if(TG_TOKEN == None):
        print("You have to pass TG_TOKEN as an environment variable")

    """Start the bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(TG_TOKEN).build()

    # on different commands - answer in Telegram
    application.add_handler(CommandHandler("start", start))

    # on voice not - send to whisper to get back with text
    application.add_handler(MessageHandler(filters.VOICE, voice))

    # Run the bot until the user presses Ctrl-C
    application.run_polling()


if __name__ == "__main__":
    main()